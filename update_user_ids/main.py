import csv
import sys
import uuid
from helpers import repo

BATCH_SIZE = 100
no_user_ids_for_mobile = open('no_user_ids_for_mobile.txt', 'a')


def _create_lead_data(lead_mobiles):
    lead_user_ids = repo.get_lead_user_ids(lead_mobiles)
    mobile_to_user_id_map = {user["mobile"]: user["id"]
                             for user in lead_user_ids}
    lead_data = []
    for mobile in lead_mobiles:
        if mobile_to_user_id_map.get(mobile) is not None:
            lead = {
                "name": "Dummy",
                "mobile": mobile,
                "user_id": mobile_to_user_id_map.get(mobile)
            }
            lead_data.append(lead)
        else:
            print(mobile, file=no_user_ids_for_mobile)

    return lead_data


def main():
    leads = repo.get_all_leads_mobile()
    lead_mobiles = []
    for lead in leads:
        lead_mobiles.append(lead["mobile"])

        if len(lead_mobiles) != BATCH_SIZE:
            continue

        lead_data = _create_lead_data(lead_mobiles)
        print(lead_data)
        response = repo.update_lead_user_id(lead_data)
        print("Batch response: ", response)
        print("\n\n")
        lead_mobiles = []

    if len(lead_mobiles):
        lead_data = _create_lead_data(lead_mobiles)
        print(lead_data)
        response = repo.update_lead_user_id(lead_data)
        print("Batch response: ", response)
        print("\n\n")


main()
